# stefantrinh - Personal Website
To see my website click the link below:  
http://www.stefantrinh.co.uk  
  
# Personal Website 2.0.0
  
## Summary  
A personal portfolio website for myself. It is an about me website which consists of my career and education history. It also displays my portfolio showing all the projects I have created or contributed on. I also have plans to introduce a blog onto the website and increase social media presence.  
  
## Contents    
- Homepage - front page acting as a site map displaying links to other parts of the website  
- About Page - About me, Technical Stack and Career History  
- Z Drive - Portfolio of all my Projects and Photography Gallery.
- Blog - A blog with social media links, posts and recommendations.
- Contact - Contact Page with all my details and enquiry form.

## Hosting Issue 
  
My current webhost for the website http://www.stefantrinh.co.uk does not have adequate support for django as there is no access to the Shell. instead of changing the webhost for which I have multiple sites on and am contracted to, I decided to host the website on pythonanywhere and had the webhost redirect the upon visiting the website to the pythonanywhere domain.  


# Personal Website 1.0.0
The first version of my website was html, css, and javascript using a file manager to organise the files. However after learning basics of django, I decided to make the switch from the file system to a web framework.
