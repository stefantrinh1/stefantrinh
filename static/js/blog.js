// ===========  Additional NavBar for the Blog Page   ===================

/* This is the function for the login and post options 
nav bar on the blogs page. used only on mobile devices
as a collaspable menu.
*/
function hamburgerExpand() {
  var x = document.getElementById("postOptions");
  console.log(x.className);
  if (x.className === 'post-options-bar navbar navbar-expand-sm') {
    x.className += " responsive";
  } else {
    x.className = 'post-options-bar navbar navbar-expand-sm'
  }
}



// =============  Cuisine and Restaurants Selectors ======================

function displayRestaurants(obj, cuisine) {
  console.log("click recieved")
  console.log(cuisine)


  // console.log(cuisine.style.display)
  let restaurant = document.getElementsByClassName(cuisine);
  let rmenu = document.getElementsByClassName("restaurantsmenu")
  let cmenu = document.getElementsByClassName("cuisinemenu")
  let backToCuisine = document.getElementsByClassName("backtocuisine")

  for (let x = 0; x < rmenu.length; x++) {
    rmenu[x].style.display = 'block'; //rmenu means restaurant-menu
  }
  for (let y = 0; y < cmenu.length; y++) {
    cmenu[y].style.display = 'none'; //cmenu means cuisine-menu
  }
  for (var i = 0; i < restaurant.length; i++) {

    if (restaurant[i].className == cuisine) {
      console.log($(restaurant.className).text())
      console.log(restaurant[i].className)
      console.log(cuisine)
      restaurant[i].style.display = 'block'
    }
  }
  for (let z = 0; z < backToCuisine.length; z++) {
    backToCuisine[z].style.display = 'block'
  }

  // let backToCuisine.addEventListener("click", back);

}


function backToCuisine() {

  const cuisineList = [".Vietnamese", ".Chinese", ".Italian", ".Indian", ".Japanese",
    ".British", ".Breakfast", ".Seafood", ".American", ".Mexican", ".Middle-Eastern"
  ]

  $(".cuisinemenu").css("display", "block");
  $(".backtocuisine").css("display", "none");
  $(".restaurantsmenu").css("display", "none");

  cuisineList.forEach((cuisine) => {
    $(cuisine).css("display", "none");
  })
}



// ==========   Instagram Gallery  ===============


// instagram api call on blog page, photos likes and comments counts,
const instaDataRequest = new XMLHttpRequest();
instaDataRequest.open("GET", "https://api.instagram.com/v1/users/self/media/recent/?=&access_token=2116304243.0917c89.a0bcdda6581d476bb24e31fcf5154f18");
instaDataRequest.onload = () => {
  const userData = JSON.parse(instaDataRequest.responseText)
  renderUserData(userData)

}
instaDataRequest.send();

const instagramSlide = document.getElementById("instagramSlide")
const renderUserData = (data) => {
  const detailsList = ['username', "website"]
  const userCounts = ["follows", 'followed_by']
  htmlString = ""


  counter = 0

  data.data.forEach(() => {
    (counter === 0) ? htmlString += htmlString += "<div class='carousel-item active'><img class='d-block w-100' src='" + data.data[counter].images.standard_resolution.url + "' height=250 width=175><small>Likes: " + data.data[counter].likes.count + "</small>" + " <strong> | </strong> " + "<small>Comments: " + data.data[counter].comments.count + "</small></div>": htmlString += "<div class='carousel-item'><img class='d-block w-100' src='" + data.data[counter].images.standard_resolution.url + "' height=250 width=175><small>Likes: " + data.data[counter].likes.count + "</small>" + " <strong> | </strong> " + "<small>Comments: " + data.data[counter].comments.count + "</small></div>"
    counter += 1
  })

  instagramSlide.insertAdjacentHTML("beforeEnd", htmlString)
}



//==================== Blog Posts ===============================


const requestPosts = new XMLHttpRequest();
const latestBlogs = document.getElementById("latestblogs"); // where the posts will be injected
const seeMorePostsBtn = document.getElementById("seemoreposts")
const currentUrl = window.location.href.replace("/blog", ""); // gets the current base URL. i.e www.stefantrinh.co.uk/ by removing the blog string.

// on page load 3 posts are automatically loaded.
requestPosts.open("GET", currentUrl + "api/postlistview/") //opens the rest api url for this site.
requestPosts.onload = () => {
  const blogPosts = JSON.parse(requestPosts.responseText); // converts the string data into JSON objects.
  renderBlogPosts(blogPosts); // runs the html render function
}
requestPosts.send(); //sends the data

// makes an ajax call when a request to see more post is clicked.
seeMorePostsBtn.addEventListener("click", () => {
  requestPosts.open("GET", currentUrl + "api/postlistview/")
  requestPosts.onload = () => {
    const blogPosts = JSON.parse(requestPosts.responseText);
    renderBlogPosts(blogPosts);
  }
  requestPosts.send();
})

let postCounter = 0 // how many active posts are showing
let loadPosts = 3 // how many posts to load.
// let seeMoreBtnActive = true  // see more button active or not.
renderBlogPosts = (data) => {

  htmlString = "" // sets an empty string ready for adding HTML.

  //produces a htmlString for each Blogpost.
  for (let i = postCounter; i < postCounter + loadPosts; i++) {
    htmlString += "<div class=''> <a class='text-warning text-expand' href='/blog/post/" + data[i].pk + "'> <h3>" +
      data[i].title + "</h3></a><br><span class='postlisttext'></span>" + data[i].text.substring(0, 280) +
      /* "<a href='/blog/post/"+
          data[i].pk+"'><br><small> comments: {{ post.approved_comments.count }}</small></a>*/
      "<div class='publishedinfo'><small>Published on:" +
      data[i].published_date + " by</small> <strong>" + data[i].author['username'] + "</strong></div></div><br><hr>"
  }

  postCounter += 3 // changes the number of posted loaded.

  // this changes the amount of posts to load to how many posts are left to load
  //to avoid going over the amount of posts.
  if (postCounter + 3 > data.length) {
    loadPosts = data.length - postCounter;
  }

  //inserts the html into the selected div.
  latestBlogs.insertAdjacentHTML("beforeend", htmlString)

  // if the number of posts loaded is equal or greater than the number of posts in the database.
  // changes the button to inactive
  if (postCounter >= data.length) {
    seeMorePostsBtn.classList.remove("text-expand");
    seeMorePostsBtn.classList.add("hide");
  }

}

//========