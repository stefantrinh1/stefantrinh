console.log("page loaded");

/* Sets the spread at which the slides in the carousal
changes automatically*/
$('.carousel').carousel({
  interval: 2000
})

/* the social Icons are for the social media buttons in the footer.
in the file storage there are two types of files for each icon.
a white one and yellow one. upon mouseover and mouse off the 
source of the icon is changed between the two. therefore the naming
of the file icon is very important. */
$(".social-icon").mouseover(function () {
  var icon = $(this).attr("src") // grabs the source of the img tag and sets to icon
  var newIcon = icon.replace("warning", "") //replaces the warning in the file name with nothing.
  $(this).attr("src", newIcon) // set the new source name with the white icon
  $(this).css("background-color", "#ffc107") // change the background colour to the yellow
})
$(".social-icon").mouseout(function () {
  var icon = $(this).attr("src")
  var newIcon = icon.replace("logo.png", "logowarning.png") //changes the last part of the file name string to the former file
  $(this).attr("src", newIcon) // sets the yellow icon back to place
  $(this).css("background-color", "") // clears any background colour
})

$(".tech-logo").mouseover(function () {
  $(this).css("-webkit-transition", "all 0.15s ease");
  $(this).css("transform", "scale(1.4)");
})
$(".tech-logo").mouseout(function () {
  $(this).css("-webkit-transition", "all 0.15s ease");
  $(this).css("transform", "scale(1.0)");
})


$(".text-expand").mouseover(function () {
  $(this).css("-webkit-transition", "all 0.5s ease");
  $(this).css("transform", "rotate(-5deg) scale(1.5)");
})
$(".text-expand").mouseout(function () {
  $(this).css("-webkit-transition", "all 0.5s ease");
  $(this).css("transform", "");
})

/* Animation. Simple Expand and Rotate on mouse over 
can be used on anything but currently being used on banners*/
$(".banner").mouseover(function () {
  $(this).css("-webkit-transition", "all 0.5s ease");
  $(this).css("transform", "rotate(-5deg) scale(1.1)");
})
$(".banner").mouseout(function () {
  $(this).css("-webkit-transition", "all 0.5s ease") // eases the animation in.
  $(this).css("transform", "rotate(+5deg)") // returns to normal size but gives it a rotated finish
})

/* Used in the footer for the email button
causes the button to highlight a different colour on mouse over. */
$(".btn-contact").mouseover(function () {
  $(this).css("color", "#efefde");
  $(this).css("border", "#efefde ridge 2px");
})
$(".btn-contact").mouseout(function () {
  $(this).css("color", "grey");
  $(this).css("border", "grey ridge 2px");
})

/* The functions below are for waypoints library.
this is used to perform a action when part of the 
page comes into view. */

/*
var $zdriveTitle = $(".zdrivetitle");
$zdriveTitle.waypoint(function() {
    // $(".zdrivetitle").css("opacity","1");
    $(".zdrivetitle").addClass("animated lightSpeedIn");
    console.log("working");
},{offset: "50%"})

var $blogTitle = $(".blogtitle");
$blogTitle.waypoint(function() {
  $(".blogtitle").css("opacity","1");
  $(".blogtitle").addClass("animated rollIn");
  console.log("working");
},{offset: "70%"})
 */

// Clearsides function goes through all the sides that are active and clears the display box.
function clearslides() {
  // if ($(".project-seftongolf").css("display")=="none") {
  //   $(".project-seftongolf").show('.project-seftongolf');}
  $(".project-seftongolf").css("display", "none");
  $(".project-connect4").css("display", "none");
  $(".project-stefantrinh").css("display", "none");
  $(".project-objectmove").css("display", "none");
  $(".project-bankinterface").css("display", "none");
}

//  clicked via the html and then chooses the right project
$(".project-tile").click(function () {
  var tile = $(this).attr("name") //collects the name of the banner that was clicked that the user wants

  if (tile === "slrphotography") {
    scrollToSlr();
  } //if the slr banner is clicked it goes to a different part of the page and the project last selected remains
  else {
    clearslides(); //clears any old project currently on display ready for the selected project to be displayed
    $(tile).hide().show(tile); //displays the newly selected project
    scrollToProject();
  } // scrolls to the project screen
})


/* Used in the Z Drive. These are the functions that controls the where 
the page scroll goes down to and how it does.*/
// scrollToProject goes to the Top of the Project Detailed View
function scrollToProject() {
  var projectScreen = document.getElementById("projectscreen")
  projectScreen.scrollIntoView({
    behavior: 'smooth',
    block: 'start'
  });
}
//This Scrolls to the Slr Part of the Page.
function scrollToSlr() {
  var slrScreen = document.getElementById("slrscreen")
  slrScreen.scrollIntoView({
    behavior: 'smooth',
    block: 'start'
  });
}

/* Used in the Zdrive page when choosing a project to look at.
When a mouseover occurs on the projects banners it will
create a layer ontop of the banner displaying the name of the 
project with a dark background*/
$(".banner-overlay").mouseover(function () {
  $(this).css("-webkit-transition", "all 0.2s ease") //eases the transition of the layer on a timer
  $(this).css("opacity", "0.9"); //changes the layer to a darker background with mouse on
})
$(".banner-overlay").mouseout(function () {
  $(this).css("opacity", "0.0"); // makes the layer completely opaque when the mouse is off it.
})