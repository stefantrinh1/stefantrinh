from django.apps import AppConfig


class CenterappConfig(AppConfig):
    name = 'centerapp'
