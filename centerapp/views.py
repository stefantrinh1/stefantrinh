"""All the Main and extra Views for the Website"""

from django.shortcuts import render

# Create your views here.

def index(request):
    """Opens the Homepage of website"""
    return render(request, "centerapp/index.html")

def centerappindex(request):
    """Opens the Homepage of website"""
    return render(request, "centerapp/index.html")

def about(request):
    """ Opens the About page"""
    return render(request, "centerapp/about.html")

def zdrive(request):
    """Opens the Portfolio Page"""
    return render(request, "centerapp/zdrive.html")

def contact(request):
    """Opens the Contact Page"""
    return render(request, "centerapp/contact.html")

def acknowledgements(request):
    """Opens The Acknowledgements Page"""
    return render(request, "centerapp/acknowledgements.html")

def htmlemail(request):
    """Opens The Marketing Html Email for my Portfolio Site"""
    return render(request, "centerapp/htmlemailprojects.html")
