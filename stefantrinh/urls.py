"""stefantrinh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import LoginView, LogoutView
from centerapp import views


urlpatterns = [
    path('', views.index, name='index'),  # Homepageview.
    path('about/', views.about, name='about'), # # About Page View
    path('centerapp/', include('centerapp.urls')), # all the main templates are stored in this app. placeholder to bring templates through.
    path('zdrive/', views.zdrive, name='zdrive'), # Portfolio/Zdrive View
    path('blog/', include('blog.urls'), name='blog'), # Blog application urls included
    path('contact/', views.contact, name='contact'), # contact page View
    path('acknowledgements/', views.acknowledgements, name='acknowledgements'), # Acknowledgements Page View
    path('admin/', admin.site.urls), # the view for the django admin page
    path('accounts/login', LoginView.as_view(), name='login'), # View for the log in page stored in centerapp
    path('accounts/logout', LogoutView.as_view(), name='logout', kwargs={'next_page':'/'}), # View for the log out in the django admin
    path('api-auth/', include('rest_framework.urls')),  # The view for the api auth
    path('api/', include('blog.api.urls'), name='api'), # the view for the blog Rest APIS
    path('htmlemail/', views.htmlemail, name='htmlemail'), # View for a sample html email for my site.
]
