"""API Views for the serialized Data"""
from django.db.models import Q
from rest_framework import generics, mixins
from blog.models import Post, Comment, Cuisine, Restaurant
from .serializers import BlogPostSerializer, CommentSerializer, CuisineSerializer, RestaurantSerializer


####### BLOG POSTS API VIEWS  #######

class PostRudView(generics.RetrieveUpdateDestroyAPIView):
    """The api RUD view for Post Objects"""
    lookup_field = 'pk'
    serializer_class = BlogPostSerializer
    permission_classes = []
    # queryset = Post.objects.all()

    def get_queryset(self):
        return Post.objects.all()

class PostListAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    """The List api view for Post Objects"""
    lookup_field = 'pk'
    serializer_class = BlogPostSerializer

    def get_queryset(self):
        qs = Post.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(Q(title__icontains=query)|Q(text__icontains=query))
        return qs

    def post(self, request, *args, **kwargs):
        """Creates a Blog Post"""
        return self.create(request, *args, **kwargs)

    # def perform_create(self, serializers):
    #     serializers.save(post=Post.pk)

class PostCreateAPIView(generics.CreateAPIView):
    """The api RUD view for Post Objects"""
    lookup_field = 'pk'
    serializer_class = BlogPostSerializer
    # queryset = Post.objects.all()

    def get_queryset(self):
        return Post.objects.all()

####### COMMENTS API VIEWS  #######

class CommentCreateAPIView(generics.CreateAPIView):
    """ The API View for creating a comment object in a post."""
    lookup_field = 'pk'
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.all()

    # def perform_create(self, serializers):
    #     serializers.save(post=Post.pk)

class CommentDestoryAPIView(generics.DestroyAPIView):
    """ The API View for creating a comment object in a post."""
    lookup_field = 'pk'
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.all()

####### Cuisine and Restaurant API VIEWS  #######


class CuisineListAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    """The List api view for Post Objects"""
    lookup_field = 'pk'
    serializer_class = CuisineSerializer

    def get_queryset(self):
        qs = Cuisine.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(Q(cuisinetype__icontains=query))
        return qs

    def post(self, request, *args, **kwargs):
        """Creates a Cuisine"""
        return self.create(request, *args, **kwargs)

class RestaurantListAPIView(mixins.CreateModelMixin, generics.ListAPIView):
    """The List api view for Post Objects"""
    lookup_field = 'pk'
    serializer_class = RestaurantSerializer

    def get_queryset(self):
        qs = Restaurant.objects.all()
        query = self.request.GET.get('q')
        if query is not None:
            qs = qs.filter(Q(name__icontains=query)|Q(city__icontains=query)|
                           Q(country__icontains=query)|Q(website__icontains=query)|Q(cuisine__cuisinetype__icontains=query))
        return qs

    def post(self, request, *args, **kwargs):
        """Creates a Cuisine"""
        return self.create(request, *args, **kwargs)
