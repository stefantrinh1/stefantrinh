"""The serializers converts models to JSON data format and validates data passed"""

from rest_framework import serializers

from blog.models import Post, Comment, Cuisine, Restaurant

class BlogPostSerializer(serializers.ModelSerializer):
    """converts Post Model to JSON data format"""

    class Meta:
        model = Post
        fields = [
            "pk",
            "author",
            "title",
            "text",
            "created_date",
            "published_date",
        ]
        depth = 1
        read_only_fields = ['pk', 'created_date'] # prevents editing

    def validate_title(self, value):
        """Validates the title checking if it is unique"""
        qs = Post.objects.filter(title__iexact=value)#gets all of the post objects titles are = value

        if self.instance:# you cannot create a Post called Post because you can creating a instance of itself
            qs = qs.exclude(pk=self.instance.pk) # this takes out the Post in the qs. via the exclude.
            # its unlikely anyone will name the title of their blog Post but just in case
        if qs.exists(): # if there is a object that was filtered
            raise serializers.ValidationError("Title has already be used. Please choose another title.")
            # raise valaution error
        return value # if not just return the value.

class CommentSerializer(serializers.ModelSerializer):
    """ Converts Comment Model to JSON data format"""
    class Meta:
        model = Comment
        fields = [
            "pk",
            "post",
            "author",
            "text",
            "created_date",
        ]
        read_only_fields = ['pk', 'post', 'created_date'] # prevents editing

class CuisineSerializer(serializers.ModelSerializer):
    """ Converts Cuisine Model to JSON data format"""
    class Meta:
        model = Cuisine
        fields = [
            'pk',
            'cuisinetype'
        ]
        read_only_fields = ['pk']

class RestaurantSerializer(serializers.ModelSerializer):
    """ Converts Restaurant Model to JSON data format"""
    class Meta:
        model = Restaurant
        fields = [
            'pk',
            'cuisine',
            'name',
            'city',
            'country',
            'website',
        ]
        read_only_fields = ['pk']
