"""stefantrinh API URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from .views import (PostRudView, PostListAPIView, PostCreateAPIView,
                    CommentCreateAPIView, CommentDestoryAPIView,
                    CuisineListAPIView, RestaurantListAPIView)



app_name = 'api'
urlpatterns = [
    path('createpost/', PostCreateAPIView.as_view(), name="post_api_create"), # Creates Post but Also a create Post option in the RUD aswell.
    path('postlistview/', PostListAPIView.as_view(), name="post_api_list"), # displays all the Blog Posts in a list
    path('<int:pk>/postrudview/', PostRudView.as_view(), name="post_api_rud"),# displays the Individual Blog Posts and attributes in Json
    path('<int:pk>/addcomment/', CommentCreateAPIView.as_view(), name="comment_api_add"), # creates a comment in a specific post
    path('<int:pk>/deletecomment/', CommentDestoryAPIView.as_view(), name="comment_api_delete"), # deletes a comment in a specific post
    path('cuisinelist/', CuisineListAPIView.as_view(), name="cuisinelist"), # API view for the cuisine list.
    path('restaurantlist/', RestaurantListAPIView.as_view(), name="restaurantlist") # API view for the Restaurant list.
    ]
