""" Configuration names for All the apps """
from django.apps import AppConfig


class BlogConfig(AppConfig):
    """Configures the blog app and gives it the name blog."""
    name = 'blog'
