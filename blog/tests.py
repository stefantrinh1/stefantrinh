"""Api Tests"""

# Create your tests here.


from django.contrib.auth import get_user_model
from django.test import TestCase
from rest_framework.test import APITestCase
from blog.models import Post
# Create your tests here.

User = get_user_model()

class PostTestCase(APITestCase):
    def setUp(self):
        user = User(username='testxxxuser', email='test@test.co.uk')
        user.set_password('testpassword')
        user.save()
        blog_posts = Post.objects.create(
            author=user,
            title="new title",
            text="test content",
        )

def test_one_user(self):
    user_count = User.objects.count()
    self.assertEqual(user_count, 1)
