"""stefantrinh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.urls import path
from blog.views import (PostListView, PostDeleteView,
                        PostDetailView, PostUpdateView,
                        DraftListView, CreatePostView,
                        comment_remove,
                        post_publish,
                        NewCuisineView, NewRestaurantView,)
                        # CommentFormView,add_comment_to_post,comment_approve,)

# path('commentform/', CommentFormView.as_view(), name='comment_form'),
# path('post/<int:pk>/comment',add_comment_to_post , name='add_comment_to_post'),
# path('comment/<int:pk>/approve/',comment_approve, name='comment_approve'),
urlpatterns = [
    path('', PostListView.as_view(), name='post_list'),
    path('post/<int:pk>/', PostDetailView.as_view(), name='post_detail'),
    path('drafts/', DraftListView.as_view(), name='post_draft_list'),
    path('post/new/', CreatePostView.as_view(), name='post_new'),
    path('post/<int:pk>/edit/', PostUpdateView.as_view(), name='post_edit'),
    path('post/<int:pk>/remove/', PostDeleteView.as_view(), name='post_remove'),
    path('post/<int:pk>/publish/', post_publish, name='post_publish'),
    path('comment/<int:pk>/remove/', comment_remove, name='comment_remove'),
    path('newcuisine/', NewCuisineView.as_view(), name='new_cuisine'),
    path('newrestaurant/', NewRestaurantView.as_view(), name='new_restaurant'),
    ]
