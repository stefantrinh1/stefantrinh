"""Views for The Blog Application."""

from rest_framework import generics
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.views.generic.edit import FormMixin
from django.utils import timezone
from django.urls import reverse_lazy, reverse
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (ListView, DetailView, CreateView, UpdateView,
                                  DeleteView, FormView)
from blog.forms import PostForm, CommentForm, NewCuisineForm, NewRestaurantForm
from blog.models import Post, Comment, Cuisine, Restaurant

# from itertools import chain
# result_list = list(chain(page_list, article_list, post_list))


# Create your views here.


class PostListView(ListView):
    """Creates a list view of all the blog posts"""
    template_name = Post
    context_object_name = ''

    def get_context_data(self, **kwargs):
        context = super(PostListView, self).get_context_data(**kwargs)
        # numcomments = self.approved_comments.objects.count
        # stores the extra context in a dictionary ready to be called in the templates
        context.update({
            'cuisine_objects': Cuisine.objects.order_by('cuisinetype'),#adds Cuisine Obj to view
            'restaurant_objects': Restaurant.objects.order_by('name'),#adds resturant obj to view
            'popular_post_objects': Post.objects.order_by('title'),

        })
        return context

    def get_queryset(self):
        """get the post objects from the database in order by published date."""
        return Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')[0:5]
        # q1 = Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')[0:5]
        # q2 = Post.objects.order_by('title')
        # qs = q1|q2
        # # qs.extend([q1, q2])
        # return qs
        # keep its to 5 newest posts


# class PopularPosts(ListView):
#     """Returns the posts in order by the number of approved comments/active comments"""
#     template_name = Post
#     context_object_name = 'popular_post_objects'

#     def get_queryset(self):
#         return Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')[0:5]
#         # sorted_results = sorted(unsorted_results, key= lambda t: t.())
#         # """grabs 5 posts with the most approved comments"""
#         # return Post.objects.all.order_by('title') #keeps it to 5 popular posts


class PostDetailView(FormMixin, DetailView):
    """returns the detailed view of the chosen blog via clicking the link to it
    using the pk retrived from that."""

    model = Post
    form_class = CommentForm

    def get_success_url(self):
        return reverse('post_detail', kwargs={'pk': self.object.id})
        # refreshes the page of which post it is currently on when action is success.

    def get_context_data(self, **kwargs):
        context = super(PostDetailView, self).get_context_data(**kwargs)
        context['form'] = CommentForm(initial={'post': self.object.id})
        return context
        # grabs the form from the formclass and all the posts but sets up theinitial as the
        # post object it is currently on and hides the field from users so they can only post
        # comments on the current post page. the field is hidden in the forms.py

    def post(self, request, *args, **kwargs):
        """Gets the valid comment form and posts the comment made."""
        self.object = self.get_object()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def form_valid(self, form):
        """ This is the function that saves the Grabbed Comment Form"""
        form.save()

        return super(PostDetailView, self).form_valid(form)


class CommentFormView(FormView):
    """The View to Show the Comment Form"""
    template_name = 'blog/post_detail.html'
    form_class = CommentForm
    success_url = reverse_lazy('blog/post_detail.html')

    def __str__(self):
        return "comment form"



class CreatePostView(CreateView, LoginRequiredMixin):
    """A view that allows the user to create a new blog Post"""
    login_url = '/login/'
    redirect_field_name = 'blog/post_detail.html'
    form_class = PostForm
    model = Post




class PostUpdateView(UpdateView, LoginRequiredMixin):
    """An view that allows the user to update the blog Post content."""
    login_url = '/login/'
    redirect_field_name = 'blog/post_detail.html'
    model = Post
    form_class = PostForm




class PostDeleteView(DeleteView, LoginRequiredMixin):
    """Shows a delete View of the Post confirming the delete"""
    login_url = '/login/'
    model = Post
    success_url = reverse_lazy("post_list")




class DraftListView(ListView, LoginRequiredMixin):
    """Shows A list view of all the Draft Posts"""
    login_url = '/login/'
    template_name = "post_draft_list.html"
    redirect_field_name = 'blog/post_draft_list.html'
    model = Post
    def get_queryset(self):
        return Post.objects.filter(published_date__isnull=True).order_by('created_date')
        # grabs all the draft posts that does not have publish date and orders them by create data



@login_required
def post_publish(request, pk):
    """Gets Post Object and then activate The oBjects Publish Method"""
    post = get_object_or_404(Post, pk=pk) #grabs Post Object by PK
    post.publish() #activates the Post Method to add a publish date to the Post Object
    return redirect('post_detail', pk=pk)

@login_required
def comment_remove(request, pk):
    """Removes a comment from the Post Object and then Delete the Comment Object."""
    comment = get_object_or_404(Comment, pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    return redirect('post_detail', pk=post_pk)




class NewCuisineView(CreateView, LoginRequiredMixin):
    """The View to Add a new Cuisine to the DB using a Form"""
    login_url = '/login/'
    redirect_field_name = 'blog/post_list.html'
    form_class = NewCuisineForm
    context_object_name = 'cuisine_objects'
    # gives the context object a useable name in other classes like the postlistview class.
    model = Cuisine

    def get_context_data(self, **kwargs):
        context = super(NewCuisineView, self).get_context_data(**kwargs)
        context.update({
            'cuisine_objects': Cuisine.objects.order_by('cuisinetype'),
            # gets all the cuisine objects and gives it a key for access.
        })
        return context

    def get_queryset(self):
        return Cuisine.objects.order_by('cuisinetype')




class NewRestaurantView(CreateView, LoginRequiredMixin):
    """The View to Add a new restaurant to the DB using a Form"""
    login_url = '/login/'
    redirect_field_name = 'blog/post_list.html'
    form_class = NewRestaurantForm
    context_object_name = 'restaurant_objects'
    model = Restaurant
    def get_queryset(self):
        return Restaurant.objects.order_by('name')




#This is now legacy code that used when the comment form was on a seperate view to the post.
# def add_comment_to_post(request, pk):
#     post = get_object_or_404(Post, pk=pk)
#     if request.method == "POST":
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             comment = form.save(commit=False)
#             comment.post = post
#             comment.save()
#             return redirect('post_detail', pk=post.pk)
#     else:
#         form = CommentForm()
#     return render(request, 'blog/comment_form.html', {'form':form})


# this is a optional comment approval. Is not currently in use as
# approval of comments is not needed in this small project.
# @login_required
# def comment_approve(request, pk):
#     comment = get_object_or_404(Comment, pk=pk)
#     comment.approve()
#     return redirect('post_detail', pk=comment.post.pk)
