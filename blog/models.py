"""Models for the Blog Application"""

from django.db import models
from django.urls import reverse
from django.utils import timezone

# Create your models here.

class Post(models.Model):
    """This Model Is a Blog Post Object."""
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)
    title = models.CharField(max_length=70)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    published_date = models.DateTimeField(blank=True, null=True)

    def publish(self):
        """changes the publish date to now saves the changes to the Post Object"""
        self.published_date = timezone.now()
        self.save()
    def approved_comments(self):
        """filters and returns all the approved comments"""
        return self.comments.filter(approved_comment=True)

    def get_absolute_url(self):
        """gets the url to push the model to."""
        return reverse("post_detail", kwargs={"pk": self.pk})

    def __str__(self):
        return self.title



class Comment(models.Model):
    """This Model Is a Comment Object"""
    post = models.ForeignKey("blog.Post", related_name="comments", on_delete=models.CASCADE)
    author = models.CharField(max_length=40)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=True)
    # if you want to approve comments before they go live change True to False
    # reactivate the function in views and the url path.

    def get_absolute_url(self):
        """gets the url and pk of the post to push the Comment object to."""
        return reverse("post_list", kwargs={"pk": self.pk})

    def approve(self):
        """changes the comment to approve if it false"""
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text



class Cuisine(models.Model):
    """This Model Is a Cuisine Object."""
    cuisinetype = models.CharField(max_length=40)

    def get_absolute_url(self):
        """gets the url to push the Cuisine model to."""
        return reverse("new_cuisine",)

    def __str__(self):
        return self.cuisinetype


class Restaurant(models.Model):
    """This Model Is a Resturant Object."""
    cuisine = models.ForeignKey("blog.Cuisine", related_name="restaurant", on_delete=models.CASCADE)
    name = models.CharField(max_length=80)
    city = models.CharField(max_length=80)
    country = models.CharField(max_length=50)
    website = models.URLField(max_length=200, default="https://")

    def get_absolute_url(self):
        """gets the url to push the Resturant model to."""
        return reverse("new_restaurant",)

    def __str__(self):
        return self.name
