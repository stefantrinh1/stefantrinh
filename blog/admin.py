""" Register all your models here. """

from django.contrib import admin
from blog.models import Post, Comment, Cuisine, Restaurant
# Register your models here.

admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Cuisine)
admin.site.register(Restaurant)
