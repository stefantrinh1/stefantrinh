"""All the forms for the blog application created from Models."""

from django import forms
from blog.models import Post, Comment, Cuisine, Restaurant

class PostForm(forms.ModelForm):
    """form to create a new blog post"""
    class Meta:
        model = Post
        fields = ('author', 'title', 'text')
        widgets = {
            "title": forms.TextInput(attrs={'placeholder':'Title', 'class':'form-input-box'}),
            "text": forms.Textarea(attrs={'data-placeholder':'Text',
                                          'class':'form-input-box editable'}),
        }
        labels = {
            'text': (''),
            'title': (''),
        }

class CommentForm(forms.ModelForm):
    """form to create a new comment on a post."""
    class Meta:
        model = Comment
        fields = ('author', 'text', "post")
        widgets = {
            "author": forms.TextInput(attrs={'cols': 80, 'rows': 1,
                                             'class':'form-input-box', 'placeholder':'Name'}),

            "text": forms.Textarea(attrs={'data-placeholder':'Comment', 'cols': 80, 'rows': 1,
                                          'class':'form-input-box editable'}),
            'post': forms.HiddenInput(),
            # when using the editor the place holder needs to be named data-placeholder instead
            # of placeholder editable is the copy medium app. it give the textfield some bases
            # to style text
        }
        labels = {
            'text': (''), # removes any label on the form field
            'author': (''),
        }


class NewCuisineForm(forms.ModelForm):  # for to create a new cuisine
    """This is the form to enter a new type of cuisine you recommend."""
    class Meta:
        model = Cuisine
        fields = ('cuisinetype',)
        widgets = {
            "cuisinetype": forms.TextInput(attrs={'class':'form-input-box'}),
            }

class NewRestaurantForm(forms.ModelForm): # form to create a new Resturant
    """This is the form to enter a new resturant recommendation."""
    class Meta:

        model = Restaurant
        fields = ("cuisine", "name", "city", "country", "website")
        widgets = {
            "name": forms.TextInput(attrs={'class':'form-input-box'}),
            "city": forms.TextInput(attrs={'class':'form-input-box'}),
            "country": forms.TextInput(attrs={'class':'form-input-box'}),
            "website": forms.TextInput(attrs={'class':'form-input-box'}),
            }
